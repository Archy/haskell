import Control.Monad  

main = interact $ unlines . map isPalindorme . lines
    where   isPalindorme s 
                | s == reverse s = "palindromer"
                | otherwise = "not palindorme"

