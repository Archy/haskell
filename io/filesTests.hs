import System.IO

-- main = do
--     handle <- openFile "testFile.txt" ReadMode
--     contest <- hGetContents handle
--     putStr contest
--     hClose handle

-- same but withFile:
main = do
    withFile "testFile.txt" ReadMode (\handle -> do
        contest <- hGetContents handle
        putStr contest)