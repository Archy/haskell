import Control.Monad  
import Data.Char  

-- Stream test      
-- main = forever $ do  
--     putStrLn "Give me some input: "  
--     c <- getContents
--     putStrLn "Testing"  
--     putStrLn $ map toUpper c

-- Stream with transformations tests
-- main = do  
--     contents <- getContents  
--     putStr (shortLinesOnly contents)  
    
-- shortLinesOnly :: String -> String  
-- shortLinesOnly input =   
--     let allLines = lines input  
--         shortLines = filter (\line -> length line < 10) allLines  
--     in  unlines shortLines   