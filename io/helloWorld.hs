-- complie with ghc --make helloWorld.hs
-- run helloWorld.exe

import Data.Char (toUpper)

main = do 
    putStrLn("Hello World!")
    putStrLn("Hello World again!")
    
    name <- getLine
    let upperName = map toUpper name
    putStrLn $ "Hello " ++ upperName

    ioString <- ioFromString
    putStrLn ioString

ioFromString :: IO String
ioFromString = return "I'm an IO from a pure String!"