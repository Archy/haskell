-- converts arithmetic expression to Reverse Polish Notation, 
-- computes the RNP and saves everything to a file
import System.IO 
import Debug.Trace
import Data.Char (digitToInt, isSpace, isDigit)
import Data.List (dropWhileEnd)

data Expression = Number { number :: Int } | Symbol { symbol :: Char } deriving Eq
instance Show Expression where  
    show (Number n) = show n
    show (Symbol s) = show s

main = do
    withFile "rnp.txt" ReadMode (\handle -> do
        contents <- hGetContents handle
        putStr (unlines . map convertToRnpAndEvaluate . lines $ contents))

convertToRnpAndEvaluate :: String -> String
convertToRnpAndEvaluate expression = show rnp ++ " = " ++ show (evaluateRnp rnp [])
    where rnp = toRnp (splitExperssion expression) []

splitExperssion :: String -> [Expression]
splitExperssion [] = []
splitExperssion l@(x:xs)
    | isSpace x = splitExperssion xs
    | isDigit x = let n = takeWhile isDigit l
                    in (Number (read n)) : splitExperssion (drop (length n - 1) xs)
    | otherwise = (Symbol x) : splitExperssion xs

toRnp :: [Expression] -> [Expression] -> [Expression]
toRnp [] stack = stack
toRnp (x@(Number n):xs) stack = x : toRnp xs stack
toRnp (x@(Symbol s):xs) stack
    | s == '(' = toRnp xs (x : stack)
    | s == ')' = let    symbols = takeWhile (/=(Symbol '(')) stack
                        newStack = (drop (length symbols + 1) stack)
                    in symbols ++ toRnp xs newStack
    | otherwise = let   symbols = takeWhile (\other -> symbolPriority other >= symbolPriority x) stack
                        newStack = x : (drop (length symbols) stack)
                    in symbols ++ toRnp xs newStack
    where symbolPriority (Symbol s)
            | s == '('  = 0
            | s == '+' || s == '-' = 1
            | s == '*' || s == '/' = 2
            | otherwise = error ("Not a valid operator: " ++ show s)

evaluateRnp :: [Expression] -> [Int] -> Int
evaluateRnp [] [x] = x
evaluateRnp ((Number n):xs) stack = evaluateRnp xs (n:stack)
evaluateRnp ((Symbol s):xs) (a:b:stack) = evaluateRnp xs (result:stack)
    where   result = case s of
                        '-' -> b - a
                        '+' -> b + a
                        '*' -> b * a
                        '/' -> b `div` a
