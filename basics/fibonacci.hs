-- enable time measure with :set +s in ghci

-- naive implementation
import Debug.Trace (trace)
naiveFibonacci :: Int -> Int
naiveFibonacci n =
    case n of
        0 -> 0
        1 -> 1
        _ -> (naiveFibonacci $ n - 1) + (naiveFibonacci $ n - 2)

-- implementation with dynamic programming

fibonacci :: Int -> Int
fibonacci n = (createFibonacciList n []) !! n

createFibonacciList :: Int -> [Int] -> [Int]
createFibonacciList n fl
    | x == l = fl
    | x > l = createFibonacciList n (countNext fl)
    where   l = length fl
            x = n + 1

countNext :: [Int] -> [Int]
countNext [] = [0]
countNext l@([_]) = l ++ [1]
countNext l = l ++ [next]
    where   n = length l - 1
            next = ((l !! n) + (l !! (n-1)))


-- another implementation with dynamic programming

fibonacci2 :: Int -> Int
fibonacci2 0 = 0
fibonacci2 1 = 1
fibonacci2 n = countFibonnacci2 (1, 0) 1 n

countFibonnacci2 :: (Int, Int) -> Int -> Int -> Int
countFibonnacci2 (f1, f2) n nTarget
    | n == nTarget = f1
    | n < nTarget = countFibonnacci2 (f1+f2, f1) (n+1) nTarget