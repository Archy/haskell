import Data.Map (Map)
import Debug.Trace (trace)
data MyType = A Int| B | C

data Point = Point Float Float deriving (Show)  
data Shape = Circle Point Float | Rectangle Point Point deriving (Show)  

surface :: Shape -> Float  
surface (Circle _ r) = pi * r ^ 2  
surface (Rectangle (Point x1 y1) (Point x2 y2)) = (abs $ x2 - x1) * (abs $ y2 - y1)  

data Vehicle = Car { name:: String, model :: String } | Boat { name :: String, year :: Int } deriving (Show)

-- type parameters

data Either' a b = Left' a | Right' b

-- type synonyms

type Name = String
type EitherString = [Either' String String]
type IntMap v = Map Int v

-- resursive data structures

data List a = Empty | Cons {listHead:: a, listTail :: List a} deriving (Show, Read, Eq, Ord)

createList :: List Int
createList = 3 `Cons` (2 `Cons` (1 `Cons` Empty))

infixr 5 :-: 
data List2 a = Empty2 | a :-: List2 a deriving (Show, Read, Eq, Ord)

createList2 :: List2 Int
createList2 = 3 :-: 2 :-: 1 :-: Empty2 

infixr 5 .++
(.++) :: List2 a -> List2 a -> List2 a
Empty2 .++ ys = ys
(x :-: xs) .++ ys = x :-: (xs .++ ys)

-- binary search tree

data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show, Read, Eq)

leaf :: a -> Tree a
leaf a = Node a EmptyTree EmptyTree

treeInsert :: Ord a => Show a => Tree a -> a -> Tree a
treeInsert EmptyTree x = leaf x
treeInsert (Node n left right) x 
    | n == x = Node x left right
    | x < n = Node n (treeInsert left x) right
    | otherwise = Node n left (treeInsert right x)

treeContains :: Ord a => Tree a -> a -> Bool
treeContains EmptyTree x = False
treeContains (Node n left right) x
    | n == x = True
    | x < n = treeContains left x
    | otherwise = treeContains right x

-- typeclasses

data TrafficLight = Red | Yellow | Green  

instance Eq TrafficLight where  
    Red == Red = True  
    Green == Green = True  
    Yellow == Yellow = True  
    _ == _ = False  

instance Show TrafficLight where  
    show Red = "Red light"  
    show Yellow = "Yellow light"  
    show Green = "Green light"  

-- Anything is bool example (call it with: booleable <variable>)

class Booleable a where
    booleable :: a -> Bool

instance Booleable [a] where
    booleable [] = False
    booleable _ = True

instance Booleable Int where
    booleable 0 = False
    booleable _ = True

instance Booleable (Tree a) where  
    booleable EmptyTree = False  
    booleable _ = True 

-- functor typeclass 

-- call it with `fmap (function) tree`
instance Functor Tree where
    fmap f EmptyTree = EmptyTree
    fmap f (Node x left right) = Node (f x) (fmap f left) (fmap f right)

