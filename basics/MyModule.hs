module MyModule 
(
    publicFoo,
    privateFooAccessor
) where

publicFoo :: String
publicFoo = "I'm a public function"

privateFooAccessor :: String
privateFooAccessor = privateFoo

privateFoo :: String
privateFoo = "I'm a private function"