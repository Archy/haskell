import Debug.Trace (trace)
doubleMe x = x + x
doubleUs x y = x*2 + y*2  

doubleSmallNumber x = if x > 100 then x 
                        else x*2

-- lists:

doubleList l = 
    if null l then []
    else (doubleMe (head l)) : (doubleList (tail l))

boomBang l = [ if (mod x 2) == 0 then "boom!" else "bang!" | x <- l, (mod x 3) == 0, x /= 3 ]

multipleList l1 l2 = [ x * y | x <- l1, y <- l2, x /= 2]

length' l = sum[1 | _ <- l]

zipFoo = zip [1..] ["apple", "orange", "cherry", "mango"]  

-- types:

sumList :: Num a => [a] -> a
sumList l = sum l

sumList2 :: [Int] -> Int  
sumList2 l = sum l

readInt :: [Char] -> Int
readInt x = read x  :: Int

-- functions

fibbonaci :: Int -> Int
fibbonaci 0 = 0
fibbonaci 1 = 1
fibbonaci n = fibbonaci (n - 1) + fibbonaci (n - 2)

addPairs :: Num a => (a, a) -> (a,a) -> (a,a)
addPairs (x1, y1) (x2, y2) = (x1+x2, y1+y2)

doubleList' :: Num a => [a] -> [a]
doubleList' [] = []
doubleList' (h:t) = (doubleMe h) : (doubleList' t) 

listInfo :: Show a => [a] -> String
listInfo [] = "The list is empty"
listInfo (x:[]) = "The list has 1 element " ++ show x
listInfo (x:y:[]) = "The list has two elements: " ++ show x ++ " and " ++ show y
listInfo (x:y:_) = "This list is long. The first two elements are: " ++ show x ++ " and " ++ show y 

firstLetter :: String -> String
firstLetter [] = "The string is empty"
firstLetter str@(x:xs) = "The first letter of " ++ str ++ " is " ++ [x]  

-- infix function
add' :: Int -> Int -> Int
a `add'` b = a + b

-- guards

rangeGuard :: Int -> String
rangeGuard x
    | x < 10 = "Less than 10"
    | x < 20 = "Less than 20"
    | otherwise = "More than 20"

bmiTell :: (RealFloat a) => a -> a -> String  
bmiTell weight height  
    | bmi <= 18.5 = "Underweight"  
    | bmi <= 25.0 = "Normal"  
    | bmi <= 30.0 = "Overweight"  
    | otherwise = "Obese"
    where bmi = weight / height ^ 2

-- where and let
sumSquare :: Num a => a -> a -> a
sumSquare a b = a2 + ab + b2
    where   a2 = a ^ 2
            b2 = b ^ 2
            ab = 2* a * b 


sumSquare' :: Num a => a -> a -> a
sumSquare' a b =
    let sqr x = x ^ 2
    in let  a2 = sqr a
            b2 = sqr b
            ab = 2 * a * b
        in a2 + ab + b2

letInList :: [[Int]]
letInList = [let square x = x * x in [square 5, square 3, square 2]]  

doubleList3 :: Integral a => [a] -> [a]
doubleList3 l = [ dub | x <- l, let dub = 2*x, let isEven t = mod x 2 == 0 in isEven x]

-- case

fibbonaci2 :: Int -> Int
fibbonaci2 n = 
    case n of 
        0 -> 1
        1 -> 1
        _ -> fibbonaci2 (n-1) + fibbonaci2 (n-2)

listInfo2 :: Show a => [a] -> String
listInfo2 l = "The list is " ++ 
    case l of 
        [] -> "empty"
        [_] -> "a single list"
        _ -> "long list"


-- recursion

minimum' :: Ord a => [a] -> a
minimum' [h] = h
minimum' (h:t)
    | h < minT = h
    | otherwise = minT
    where minT = minimum' t

replicate' :: Integral a => (b, a) -> [b]
replicate' (b, 1) = [b]
replicate' (b, n) = b : replicate' (b, n-1)

-- merge sort

mergeSort :: Ord a => [a] -> [a]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort l = merge (mergeSort l1) (mergeSort l2)
    where (l1, l2) = splitAt (div (length l) 2) l

merge :: Ord a => [a] -> [a] -> [a]
merge l1 [] = l1
merge [] l2 = l2
merge l1@(l1h:l1t) l2@(l2h:l2t)
    | l1h < l2h = l1h : merge l1t l2
    | otherwise = l2h : merge l1 l2t

-- permutations

permutations :: Ord a => [a] -> [[a]]
permutations [] = [[]]
permutations [x] = [[x]]
permutations l = [ h:tp | (h, t) <- headPermutations l, tp <- permutations t]

headPermutations :: Ord a => [a] -> [(a, [a])]
headPermutations [x] = [(x, [])]
headPermutations l = [ (l !! i, removeElement l i) | i <- [0..(length l) - 1]]

removeElement :: Ord a => [a] -> Int -> [a]
removeElement l i =
    let (h,t) = splitAt i l
    in h ++ (tail t)

--curried functions

compareWithHundred :: (Num a, Ord a) => a -> Ordering  
compareWithHundred x = compare 100 x

divideByTen :: (Floating a) => a -> a  
divideByTen = (/10)  

testOrder :: String -> String -> String -> String 
testOrder a b c = a ++ b ++ c

-- high order function

applyNtimes :: (a -> a) -> a -> Int -> a
applyNtimes f x 1 = f x
applyNtimes f x n = f (applyNtimes f x (n-1))

zipLists :: [a] -> [b] -> (a -> b -> c) -> [c]
zipLists [] _ _ = []
zipLists _ [] _ = []
zipLists (x:xs) (y:ys) f = (f x y) : zipLists xs ys f

-- maps and filters

quicksort :: (Ord a) => [a] -> [a]    
quicksort [] = []    
quicksort (x:xs) =     
    let smallerSorted = quicksort (filter (<=x) xs)  
        biggerSorted = quicksort (filter (>x) xs)   
    in  smallerSorted ++ [x] ++ biggerSorted  


collatzChain :: Integral a => a -> [a]
collatzChain 1 = [1]
collatzChain n 
    | even n = n : collatzChain (div n 2)
    | otherwise = n : collatzChain (n*3 + 1)  

chainsLongerThan :: Int -> Int
chainsLongerThan n = length(filter isLongerThan (map collatzChain [1..100]))
    where isLongerThan c = (length c) > n

multiplyBy :: Int -> Int -> Int
multiplyBy x n = (multiplicationList !! n) x
    where multiplicationList = map (*) [1..]

-- lambdas