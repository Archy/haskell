import Data.Map (Map)
import Graph
import Queue
import System.IO
import Debug.Trace (trace)

main = do 
    putStrLn("loading graph")
    edges <- loadGraph "graph.txt"
    putStrLn("graph loaded")

    putStrLn("Enter start node")
    src <- fmap (\x -> read x :: Int) getLine

    putStrLn("Enter destination node")
    dst <- fmap (\x -> read x :: Int) getLine

    putStrLn("From " ++ show src ++ " to " ++ show dst)
    let result = dijkstra edges Empty (Item src 0 Empty) dst

    putStrLn(show result)
    
dijkstra :: [[Edge]] -> Node Int Int -> Node Int Int -> Int -> Maybe Int
dijkstra edges q u dst =
    case nextU of
        Nothing -> Nothing
        Just (nextU') -> let nextNode = item nextU' in 
            case () of
                ()  | dst == nextNode -> Just (priority nextU')
                    | otherwise -> trace (show newQ) dijkstra edges newQ nextU' dst
    where
        currentNode = item u
        currentDist = priority u  
        updateQueueWithEdge' = updateQueueWithEdge currentDist
        tmpQ = foldl updateQueueWithEdge' q (edges !! currentNode)
        (nextU, newQ) = popQueue tmpQ 

updateQueueWithEdge :: Int -> Node Int Int -> Edge -> Node Int Int
updateQueueWithEdge currentDistance q edge = pushOrUpdateQueue q node newDistance
    where 
        node = destination edge
        newDistance = lenght edge + currentDistance