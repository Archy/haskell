module Queue (
    Node(Empty, Item, item, priority),
    pushQueue, pushOrUpdateQueue, popQueue, isQueueEmpty
) where

data Node a b = Empty | Item { item :: a, priority :: b, next :: Node a b} deriving Show

-- foldl (\head x -> pushHeap head (fst x) (snd x)) Empty [(1,1),(3,3),(2,2),(10,10),(9,9),(8,8),(7,7)]
pushQueue :: (Ord p, Eq a) => Node a p -> a -> p -> Node a p
pushQueue Empty x priority = Item x priority Empty
pushQueue head x xPriority
    | xPriority < (priority head) = Item x xPriority head
    | otherwise = Item (item head) (priority head) (pushQueue (next head) x xPriority)

pushOrUpdateQueue :: (Ord p, Eq a) => Node a p -> a -> p -> Node a p
pushOrUpdateQueue Empty x priority = Item x priority Empty
pushOrUpdateQueue head x xPriority = 
    let currentPriority = getItemPriority head x in
    case currentPriority of
        Nothing -> pushQueue head x xPriority
        Just (currentPriority') -> 
            case () of
                ()  | xPriority < currentPriority' -> pushQueue newHead x xPriority
                    | otherwise -> head
                    where newHead = removeFromQueue head x
        
popQueue :: (Ord p, Eq a) => Node a p -> (Maybe (Node a p), Node a p)
popQueue Empty = (Nothing, Empty)
popQueue head = (Just head, next head)

isQueueEmpty :: (Ord p, Eq a) => Node a p -> Bool
isQueueEmpty Empty = True
isQueueEmpty _ = False


getItemPriority :: (Ord p, Eq a) => Node a p -> a -> Maybe p
getItemPriority Empty _ = Nothing
getItemPriority head x
    | item head == x =  Just (priority head)
    | otherwise = getItemPriority (next head) x

removeFromQueue :: (Ord p, Eq a) => Node a p -> a -> Node a p
removeFromQueue Empty _ = Empty
removeFromQueue head x
    | item head == x = next head
    | otherwise = Item (item head) (priority head) (removeFromQueue (next head) x)
    
