# Dijsktra algorithm implementation in Haskell

Implementation of the Dijkstra algorithm in Haskell.

Example dataset is placed in the `graph.txt` file (it's taken from https://eduinf.waw.pl/inf/alg/001_search/0138.php)