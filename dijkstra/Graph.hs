module Graph 
(
    Edge(destination, lenght),
    loadGraph
) where

import System.IO ( IOMode(ReadMode), hGetLine, withFile ) 
import Control.Monad (replicateM)

data Edge = Edge { destination :: Int, lenght:: Int } deriving Show

loadGraph :: String -> IO [[Edge]]
loadGraph filePath = do
    withFile filePath ReadMode (\handle -> do
        headerIO <- hGetLine handle
        let (nodesCount, edgesCount) = splitHeader headerIO
    
        edges <- fmap (map parseEdge) (replicateM edgesCount $ hGetLine handle)

        let graph = [ nodeEdges | n <-[0..nodesCount-1], let nodeEdges = map (\e -> snd e) $ filter (\e -> fst e == n) edges ]
        return graph)
    
splitHeader :: String -> (Int, Int)
splitHeader header = (headerSplit !! 0, headerSplit !! 1)
    where headerSplit = map (\x -> read x :: Int) $ words header
    
parseEdge :: String -> (Int, Edge)
parseEdge edgeStr = (edgeSplit !! 0, edge)
    where   edgeSplit = map (\x -> read x :: Int) $ words edgeStr
            edge = Edge (edgeSplit !! 1) (edgeSplit !! 2)